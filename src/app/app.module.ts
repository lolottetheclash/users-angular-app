import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgFlashMessagesModule } from "ng-flash-messages";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UsersComponent } from "./components/users/users.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { UserDetailComponent } from "./components/user-detail/user-detail.component";
import { UserService } from "./services/user.service";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { AddUserComponent } from "./components/add-user/add-user.component";
import { EditUserComponent } from "./components/edit-user/edit-user.component";
import { PostsComponent } from "./components/http-comps/posts/posts.component";
import { HttpClientModule } from "@angular/common/http";
import { PostFormComponent } from "./components/http-comps/post-form/post-form.component";
import { PostDetailsComponent } from "./components/http-comps/post-details/post-details.component";
import { MapComponent } from "./components/leaflet/map/map.component";
import { GmapComponent } from "./components/google/gmap/gmap.component";
import { AgmCoreModule } from "@agm/core";

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    NavbarComponent,
    UserDetailComponent,
    NotFoundComponent,
    AddUserComponent,
    EditUserComponent,
    PostsComponent,
    PostFormComponent,
    PostDetailsComponent,
    MapComponent,
    GmapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgFlashMessagesModule.forRoot(),
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: ""
    })
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule {}
