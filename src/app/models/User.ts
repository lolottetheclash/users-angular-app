export interface User {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  color?: string;
  isValid?: boolean;
}
