import { Component, OnInit } from "@angular/core";
import * as L from "leaflet";

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css"]
})
export class MapComponent implements OnInit {
  imageUrl: string;

  constructor() {
    this.imageUrl = "/assets/img/beeguard.png";
  }

  ngOnInit() {
    var mymap = L.map("mapid").setView([43.6044622, 1.4442469], 13);
    L.tileLayer(
      "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibG9sb3R0ZTMxIiwiYSI6ImNrNXNmbDRtaTA5eGMzZnE3NWRidXdiMmkifQ.iJ3QbAuxFIqoK18bFhGsFg",
      {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: "mapbox/streets-v11",
        tileSize: 512,
        zoomOffset: -1,
        accessToken: "your.mapbox.access.token"
      }
    ).addTo(mymap);
    var marker = L.marker([43.5483348, 1.5012103]).addTo(mymap);
    marker
      .bindPopup(`<img src="assets/img/beeguard.png" alt="abeilles">`)
      .openPopup();
  }
}
