import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";
import { User } from "../../models/User";
import { Observable } from "rxjs";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  users: User[];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getUsers().subscribe(users => (this.users = users));
  }

  removeUser(id: Number) {
    event.stopPropagation(); // ne prend le clic que sur l'icône voulue, et pas sur la div entière
    this.userService.deleteUser(id);
  }

  editUser(id: Number) {
    event.stopPropagation(); // ne prend le clic que sur l'icône voulue, et pas sur la div entière
    console.log(id);
  }
}
