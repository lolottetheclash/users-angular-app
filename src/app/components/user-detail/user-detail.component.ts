import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";
import { ActivatedRoute } from "@angular/router";
import { User } from "../../models/User";
import { Location } from "@angular/common";

@Component({
  selector: "app-user-detail",
  templateUrl: "./user-detail.component.html",
  styleUrls: ["./user-detail.component.css"]
})
export class UserDetailComponent implements OnInit {
  user: User;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.params["id"];
    this.userService.getUser(id).subscribe(user => (this.user = user));
  }

  goBack() {
    this.location.back();
  }
}
