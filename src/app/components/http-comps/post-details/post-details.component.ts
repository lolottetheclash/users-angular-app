import { Component, OnInit } from "@angular/core";
import { Post } from "../../../models/Post";
import { PostsService } from "../../../services/posts.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-post-details",
  templateUrl: "./post-details.component.html",
  styleUrls: ["./post-details.component.css"]
})
export class PostDetailsComponent implements OnInit {
  post: Post;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.params.id;
    this.postsService.getSinglePost(id).subscribe(post => (this.post = post));
  }
}
