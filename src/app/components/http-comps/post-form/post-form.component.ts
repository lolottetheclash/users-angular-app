import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { NgFlashMessageService } from "ng-flash-messages";
import { Post } from "../../../models/Post";
import { PostsService } from "src/app/services/posts.service";

@Component({
  selector: "app-post-form",
  templateUrl: "./post-form.component.html",
  styleUrls: ["./post-form.component.css"]
})
export class PostFormComponent implements OnInit {
  @Output() newPost: EventEmitter<Post> = new EventEmitter();
  @Output() updatedPost: EventEmitter<Post> = new EventEmitter();
  @Input() currentPost: Post; // lie le form à son parent: il récupère alors la propriété currentPost pour l'afficher
  @Input() isEdit: boolean;

  constructor(
    private ngFlashMessages: NgFlashMessageService,
    private postsService: PostsService
  ) {}

  ngOnInit() {}

  addPost(title, body) {
    if (!title || !body) {
      this.ngFlashMessages.showFlashMessage({
        messages: ["Please fill in the form"],
        timeout: 3000,
        type: "danger"
      });
    } else {
      this.postsService
        .addNewPost({ title, body } as Post)
        // emission d'un event qui inclue le nouveau post, l'event sera capté dans posts.component.html,
        // dans le app-post-form: (newPost)="onNewPost($event)", qui lancera la méthode onNewPost
        .subscribe(post => this.newPost.emit(post));
    }
  }

  updatePost() {
    this.postsService.updatePost(this.currentPost).subscribe(editedPost => {
      this.isEdit = false;
      this.updatedPost.emit(editedPost);
    });
  }
}
