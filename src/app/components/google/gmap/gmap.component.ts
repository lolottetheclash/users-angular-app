import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-gmap",
  templateUrl: "./gmap.component.html",
  styleUrls: ["./gmap.component.css"]
})
export class GmapComponent implements OnInit {
  latitude: number;
  longitude: number;
  mapType: string;
  zoom: number;

  markerLat: number;
  markerLng: number;

  constructor() {}

  ngOnInit() {
    this.latitude = 43.6044622;
    this.longitude = 1.4442469;
    this.mapType = "roadmap";
    this.zoom = 16;
  }

  onMarker(event) {
    this.markerLat = event.coords.lat;
    this.markerLng = event.coords.lng;
  }

  //     const paris = {lattitude: 48.8566969, longitude:2.3514616}
  //     const marker = new google.maps.Marker({position: uluru, map: map});
  //   }
}
