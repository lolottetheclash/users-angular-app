import { Component, OnInit, ViewChild } from "@angular/core";
import { User } from "../../models/User";
import { UserService } from "../../services/user.service";
import { ActivatedRoute } from "@angular/router";
import { NgFlashMessageService } from "ng-flash-messages";

@Component({
  selector: "app-edit-user",
  templateUrl: "./edit-user.component.html",
  styleUrls: ["./edit-user.component.css"]
})
export class EditUserComponent implements OnInit {
  user: User;
  @ViewChild("editedUser", { static: false }) form: any;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private ngFlashMessageService: NgFlashMessageService
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.params["id"];
    this.userService.getUser(id).subscribe(el => (this.user = el));
  }

  edit({ value, valid }: { value: User; valid: Boolean }) {
    if (!valid) {
      this.ngFlashMessageService.showFlashMessage({
        messages: ["Invalid Form"],
        timeout: 3000,
        type: "danger"
      });
    } else {
      value.id = this.user.id;
      this.userService.editUser(value);
      this.form.reset();
    }
  }
}
