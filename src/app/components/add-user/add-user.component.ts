import { Component, OnInit, ViewChild } from "@angular/core";
import { User } from "../../models/User";
import { UserService } from "../../services/user.service";
import { NgFlashMessageService } from "ng-flash-messages";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.css"]
})
export class AddUserComponent implements OnInit {
  user: User = {
    firstname: "",
    lastname: "",
    email: "",
    id: null
  };

  enableButton: boolean = false; // le bouton de soumiss° s'activera quand tous les champs requis sont remplis
  isHidden: boolean = true; // le form sera visible quand on clique sur le bouton add user
  @ViewChild("userForm", { static: false }) form: any;

  constructor(
    private userService: UserService,
    private ngFlashMessageService: NgFlashMessageService
  ) {}

  ngOnInit() {}

  addNewUser({ value, valid }: { value: User; valid: boolean }) {
    if (!valid) {
      this.ngFlashMessageService.showFlashMessage({
        messages: ["Form invalid"],
        timeout: 3000,
        type: "Danger"
      });
    } else {
      value.id = this.userService.users.length + 1;
      value.color = "yellow";
      this.userService.addUser(value);
      this.form.reset(); // on remet le form à 0
      this.isHidden = true; // on cache le formulaire une fois le user ajouté
      this.ngFlashMessageService.showFlashMessage({
        messages: ["User created"],
        timeout: 3000,
        type: "success"
      });
    }
  }
}
