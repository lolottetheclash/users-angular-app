import { Injectable } from "@angular/core";
import { User } from "../models/User";
import { Observable, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class UserService {
  users: User[];
  user: User;

  constructor() {
    this.users = [
      {
        id: 1,
        firstname: "Lolotte",
        lastname: "Theclash",
        email: "lolottetheclash@gmail.com",
        color: "blue",
        isValid: true
      },
      {
        id: 2,
        firstname: "Steph",
        lastname: "Gougoun",
        email: "stephGougoun@gmail.com",
        color: "orange",
        isValid: true
      },
      {
        id: 3,
        firstname: "John",
        lastname: "Doe",
        email: "jdoe@yahoo.com",
        color: "green",
        isValid: false
      }
    ];
  }

  getUsers(): Observable<User[]> {
    return of(this.users);
  }

  getUser(id: number): Observable<User> {
    const user = this.users.filter(user => user.id == id)[0];
    return of(user);
  }

  addUser(user: User) {
    this.users.unshift(user);
  }

  editUser(user: User) {
    this.users.forEach((current, index) => {
      if (current.id == user.id) {
        this.users.splice(index, 1);
      }
    });
    this.addUser(user);
  }

  deleteUser(id: Number) {
    this.users.forEach((current, index) => {
      if (current.id == id) {
        if (confirm(`Are you sure to delete ${current.firstname}?`)) {
          this.users.splice(index, 1); // splice retire du tableau '1' élément à partir de la place 'index'
        }
      }
    });
  }
}
