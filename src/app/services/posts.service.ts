import { Injectable } from "@angular/core";
import { Post } from "../models/Post";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class PostsService {
  posts: Post[];
  postsUrl: string =
    "https://jsonplaceholder.typicode.com/posts?_start=0&_end=7";

  postUrl: string = "https://jsonplaceholder.typicode.com/posts";

  constructor(private http: HttpClient) {}

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postsUrl);
  }

  addNewPost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postUrl, post, httpOptions);
  }

  updatePost(post: Post): Observable<Post> {
    const putUrl = `${this.postUrl}/${post.id}`;
    return this.http.put<Post>(putUrl, post, httpOptions);
  }

  deleteOne(post: Post): Observable<Post> {
    const url = `${this.postUrl}/${post.id}`;
    return this.http.delete<Post>(url, httpOptions);
  }

  getSinglePost(id: number): Observable<Post> {
    const url = `${this.postUrl}/${id}`;
    return this.http.get<Post>(url);
  }
}
