import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UsersComponent } from "./components/users/users.component";
import { UserDetailComponent } from "./components/user-detail/user-detail.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { EditUserComponent } from "./components/edit-user/edit-user.component";
import { PostsComponent } from "./components/http-comps/posts/posts.component";
import { PostDetailsComponent } from "./components/http-comps/post-details/post-details.component";
import { MapComponent } from "./components/leaflet/map/map.component";
import { GmapComponent } from "./components/google/gmap/gmap.component";

const routes: Routes = [
  { path: "", component: UsersComponent },
  { path: "user/:id", component: UserDetailComponent },
  { path: "edit/:id", component: EditUserComponent },
  { path: "posts", component: PostsComponent },
  { path: "posts/:id", component: PostDetailsComponent },
  { path: "leaflet", component: MapComponent },
  { path: "googleMaps", component: GmapComponent },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
